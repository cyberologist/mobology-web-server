const fetch = require('node-fetch');
const fs = require('fs');

getDataFromFile = (fileName, type) => {
    return new Promise(function (resolve, reject) {
        fs.readFile(fileName, type, (err, data) => {
            err ? reject(err) : resolve(data);
        });
    });
}
appendToFile = async (fileName, obj) => {

    await fs.appendFileSync(fileName, JSON.stringify(obj) + ',');

}
appendToJsonFile = async (fileName, obj) => {
    const fileContent = await getDataFromFile(fileName, 'utf8');
    var data = JSON.parse(fileContent);  //parse the JSON
    data.push(obj);
    await saveToJsonFile(fileName.split('.')[0], data);
}
saveToJsonFile = (title, devices) => {
    fs.writeFileSync(title + '.json', JSON.stringify(devices));
}
getContent = (url) => {
    return fetch(url)
        .then(res => res.text());
}
getCurrentTime = () => {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    return (date + ' ' + time);

}
module.exports = {
    getDataFromFile,
    saveToJsonFile,
    getContent,
    getCurrentTime,
    appendToFile,
}