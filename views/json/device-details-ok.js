module.exports = (mobile) => {
    formattedMobile = {};
    formattedMobile['id'] = mobile.id;
    formattedMobile['brand'] = mobile.brand;
    formattedMobile['name'] = mobile.model;
    formattedMobile['view'] = mobile.hits;
    formattedMobile['rating'] = mobile.rating;
    formattedMobile['image'] = mobile.smallImageUrl;
    formattedMobile['images'] = mobile.images;
    formattedMobile['reviewLink'] = mobile.reviewLink;
    formattedMobile['specs'] = mobile.specs;
    return {
        "success": true,
        message: "  😀 fetched device successfully !",
        data: formattedMobile
    };
}