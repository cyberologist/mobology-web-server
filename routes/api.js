const express = require('express');

const router = express.Router();

const mobilesRoutes = require('./mobiles');

const API_KEY = 'ILoveHadia';

router.use((req, res, next) => {
    const providedKey = req.query.api_key;
    if (providedKey && providedKey == API_KEY) {
        next();
    } else {
        if (providedKey) {
            res.status(401).json(require('../views/json/api-key-wrong.json'));
        } else {
            res.status(400).json(require('../views/json/api-key-missing.json'));
        }
    }
});

router.use('/mobiles', mobilesRoutes);

module.exports = router; 
