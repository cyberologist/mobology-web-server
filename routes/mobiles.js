const express = require('express');

const router = express.Router();

const mobilesController = require('../controllers/mobiles');

router.get('/trending', mobilesController.getTrending);

router.get('/:mobileId/details', mobilesController.getMobileDetails);

router.get('/search', mobilesController.searchByModelOrBrand);

router.get('/categories/:brand', mobilesController.getCategory);

router.get('/brands', mobilesController.getBrands);

router.post('/:mobileId/rate', mobilesController.rateMobile);


module.exports = router; 