const Sequelize = require('sequelize');
const sequelize = require('../data/database');

const brand = sequelize.define('brand', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    name: Sequelize.TEXT,
    logo: Sequelize.TEXT

});

module.exports = brand; 