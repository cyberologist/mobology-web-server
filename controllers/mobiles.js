const Mobile = require('../models/mobile');

const Brand = require('../models/brand');

const sequelize = require('sequelize');


var valuesGetter = require('sequelize-values')();

module.exports.getTrending = (req, res, next) => {


    Mobile.findAll({
        limit: 10,
        order: [
            ['hits', 'ASC']
        ],
        attributes: [
            'id', 'brand', ['model', 'name'], ['hits', 'views'], 'rating', ['smallImageUrl', 'image']
        ]
    })
        .then(mobiles => {
            res.status(200).json(require('../views/json/trending-ok.js')(mobiles));
        })
        .catch(err => {
            if (err) {
                console.log(err);
                res.status(500).json(require('../views/json/enternal-server-error.json'));
            }
        })
}

module.exports.getMobileDetails = (req, res, next) => {
    const mobileId = req.params.mobileId;

    Mobile.findByPk(mobileId)
        .then(mobile => {
            if (!mobile) {
                res.status(404).json(require('../views/json/mobile-not-found.json'));
            } else {
                mobile.images = JSON.parse(mobile.images);
                mobile.specs = JSON.parse(mobile.specs);
                mobile.localHits = Number(mobile.localHits) + 1;
                mobile.hits = Number(mobile.hits) + 1;
                mobile.save()
                    .then(result => console.log('updated viwes successfully'))
                    .catch(err => console.log('error updaing the views '));

                res.status(200).json(require('../views/json/device-details-ok.js')(mobile));
            }
        })
        .catch(err => {
            if (err) {
                console.log(err);
                res.status(500).json(require('../views/json/enternal-server-error.json'));
            }
        })
}

module.exports.searchByModelOrBrand = (req, res, next) => {

    let query = req.query.keyword;
    if (query) {
        query = query.toLowerCase();
        Mobile.findAll({
            where: {
                [sequelize.Op.or]: [
                    {
                        model:
                            sequelize.where(
                                sequelize.fn('LOWER', sequelize.col('model')), 'LIKE', '%' + query + '%')
                    },
                    {
                        brand:
                            sequelize.where(
                                sequelize.fn('LOWER', sequelize.col('brand')), 'LIKE', '%' + query + '%')
                    }
                ]
                //  [Op.like]: `%${query}%`, }

            },
            attributes: [
                'id', 'brand', ['model', 'name'], ['hits', 'views'], 'rating', ['smallImageUrl', 'image']
            ]
        })
            .then(mobiles => {
                if (mobiles.length == 0) {
                    res.status(200).json(require('../views/json/empty-search-result.json'));
                } else {
                    res.status(200).json(require('../views/json/search-results.js')(mobiles));
                }
            })
            .catch(err => {
                if (err) {
                    console.log(err);
                    res.status(500).json(require('../views/json/enternal-server-error.json'));
                }

            })
    } else {
        res.status(400).json(require('../views/json/missing-search-parameter.json'));
    }

}

module.exports.getCategory = (req, res, next) => {
    let categoryName = req.params.brand;
    categoryName = categoryName.toLowerCase();
    Mobile.findAll({ attributes: ['brand'] })
        .then(categories => {
            categories = valuesGetter
                .getValues(categories)
                .map((c) => (c.brand.toLowerCase()))
                .filter((v, i, a) => a.indexOf(v) === i);

            if (categories.includes(categoryName)) {
                Mobile.findAll({
                    where: {
                        brand: sequelize.where(
                            sequelize.fn('LOWER', sequelize.col('brand')), categoryName)
                    }, attributes: [
                        'id', ['model', 'name'], ['hits', 'views'], 'rating', ['smallImageUrl', 'image']
                    ]
                })
                    .then(devices => {
                        res.status(200).json(require('../views/json/categories-results.js')(devices));
                    })
                    .catch(err => {
                        if (err) {
                            console.log(err);
                            res.status(500).json(require('../views/json/enternal-server-error.json'));
                        }
                    })
            } else {
                res.status(422).json(require('../views/json/categories-parameter-error.json'));
            }
        })
        .catch(err => {
            if (err) {
                console.log(err);
                res.status(500).json(require('../views/json/enternal-server-error.json'));
            }
        })


}
module.exports.getBrands = (req, res, next) => {
    Brand.findAll({ attributes: ['name', 'logo'] })
        .then(brands => {
            res.status(200).json(require('../views/json/all-brands-result.js')(brands));
        })
        .catch(err => {
            if (err) {
                console.log(err);
                res.status(500).json(require('../views/json/enternal-server-error.json'));
            }
        })


}

module.exports.rateMobile = (req, res, next) => {
    const mobileId = req.params.mobileId;
    const rate = req.body.rate;
    if (!rate && rate != 0) {
        res.status(400).json(require('../views/json/missing-rate-in-body.json'));
        return;
    }
    if (isNaN(rate) || rate < 0 || rate > 5) {
        res.status(422).json(require('../views/json/invalid-rate-values.json'));
        return;
    }
    Mobile.findByPk(mobileId)
        .then(mobile => {
            mobile.rates = mobile.rates + 1;
            mobile.totalRates = mobile.totalRates + rate;
            mobile.rating = mobile.totalRates / mobile.rates;
            // console.log(valuesGetter.getValues(mobile));
            mobile.save()
                .then(result =>
                    res.status(200).json(require('../views/json/rate-posted-successfully.js')(mobile.rating)))
                .catch(err => {
                    console.log(err);
                    res.send(500).json(require('../views/json/enternal-server-error.json'));
                })
        })
        .catch(err => {
            if (err) {
                console.log(err);
                res.send(500).json(require('../enternal-server-error.json'));
            }
        })


}

