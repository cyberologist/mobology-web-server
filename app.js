const PORT = process.env.PORT || 8080;

const path = require('path');

const express = require('express');

const bodyParser = require('body-parser');

const database = require('./data/database');

// const dataGenerator = require('./data/generator/data_refresher');

// for docs 
const swaggerUi = require('swagger-ui-express');

const swaggerDocument = require('./swagger.json');

const app = express();

const apiRoutes = require('./routes/api');

app.use('/images', express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Credentials", true);
    // res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET , POST , PUT , DELETE , PATCH , DELETE');
    // res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type , Authorization');
    next();
});

app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, {
    customCss: '.swagger-ui .wrapper{ align-content:center ; width: 70%;}',
    swaggerOptions: {
        docExpansion: 'none'
    },
}));

app.use('/api', apiRoutes);

app.use((req, res, next) => {
    res.status(404).json(require('./views/json/page-not-found.json'));
})


// database.sync({ force: true })
database.sync()
    // .then(result => {
    // return dataGenerator();
    // })
    .then(result => {
        app.listen(PORT);
    });

