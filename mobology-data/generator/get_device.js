const utils = require('./utils');
const cheerio = require('cheerio');
const baseUrl = 'https://www.gsmarena.com';

module.exports.getDevice = async (url, brand) => {
    return utils.getContent(url)
        .then(async (body) => {

            const $ = cheerio.load(body);

            const modelClass = $('.specs-phone-name-title');
            const $model = $(modelClass);
            const model = $model.text();

            const hitsClass = $('.light.pattern.help.help-popularity');
            const $hits = $(hitsClass);
            const hitsText = $hits.find('span').text();
            const globalHits = hitsText.split(' ')[0];

            const fansClass = $('.light.pattern.help.help-fans');
            const $fans = $(fansClass);
            const fansText = $fans.find('a strong').text();
            const globalFans = fansText;

            const smallImageClass = $('.specs-photo-main');
            const $smallImage = $(smallImageClass);
            const smallImageUrl = $smallImage.find('a img').attr('src');

            const picsLinksIcon = $('.head-icon.icon-pictures');
            const $picsLinksIcon = $(picsLinksIcon);
            const imagesLink = baseUrl + '/' + $picsLinksIcon.parent().attr('href');
            const images = await getDevicePics(imagesLink);

            const vidClass = $('.module.module-vid-review');
            const reviewLink = vidClass.find('iframe').attr('src');

            const specsClass = $('#specs-list');
            const $specs = $(specsClass);
            const specs = [];
            $specs.children().each(function (i, element) {
                const $element = $(element);
                const $subInfo = $element.find('tbody').children();

                let singleSpec = {}; //= { 'tag': '', 'subSpecs': [] };

                let mainTag = '';
                $subInfo.each(function (index, element) {
                    const $tr = $(element);
                    if (index == 0) {
                        mainTag = formatForKey($tr.find('th').text());
                        singleSpec[mainTag] = {};
                    }
                    const title = $tr.find('.ttl').text();
                    const info = $tr.find('.nfo').text();
                    if (title && info)
                        singleSpec[mainTag][formatForKey(title.length < 2 ? mainTag : title)] = formatForValue(info);

                });
                if (Object.keys(singleSpec).length)
                    specs.push(singleSpec);
            })
            return {
                model,
                brand,
                globalHits,
                globalFans,
                smallImageUrl,
                reviewLink,
                images,
                reviewLink,
                specs
            };
        })
}
function getDevicePics(url) {
    const links = [];
    return getContent(url)
        .then(body => {
            const $ = cheerio.load(body);
            const images = $('#pictures-list').children();
            images.each(function (index, element) {
                const $element = $(element);
                const link = $element.attr('src');
                if (link)
                    links.push(link);
            })
            return links;
        })
}
function formatForKey(name) {
    let formattedName = '';
    name
        .split(' ')
        .forEach(
            (v, i) => {
                if (v === '3.5mm') {
                    v = 'threePointFivemm';
                }
                if (v === '2G') {
                    v = 'secondGeneration';
                }
                if (v === '3G') {
                    v = 'thirdGenerations';
                }
                if (v == '4G') {
                    v = 'fourthGenertation';
                }
                formattedName += (i == 0 ? v : v.charAt(0).toUpperCase() + v.substring(1, v.length));
            }
        );
    return formattedName;

}
function formatForValue(value) {
    return value.trim().replace('\\n', ' |').replace(/\s+/g, ' ');
}