const categories = require('./get_categories_links');
const cheerio = require('cheerio');
const baseUrl = 'https://www.gsmarena.com/';
const utils = require('./utils');

module.exports.getDevicesLinks = async () => {
    const categoriesLinks = await categories.getCategoriesLinks();
    const devicesLinks = [];
    for (let i = 0; i < categoriesLinks.length; i++) {
        const brand = categoriesLinks[i]['brand'];
        const links = categoriesLinks[i]['links'];

        console.log('extracting ' + brand + ' devices links.. ');

        const brandLinks = [];
        for (let j = 0; j < links.length; j++) {
            const link = links[j];

            console.log(' extracting devices links for page ' + (j + 1) + '/' + links.length);
            let devicesLinksFromOnePage;
            try {
                devicesLinksFromOnePage = await getDevicesLinksFromCategoryPage(link);
            } catch (err) {
                if (err) {
                    console.log(err);
                    j--;
                    continue;
                }
            }
            console.log((j + 1) + ' done.');

            brandLinks.push(...devicesLinksFromOnePage);
        }

        console.log((i + 1) + '/' + categoriesLinks.length + ' - ' + brand + ' done . ');

        devicesLinks.push({
            brand,
            brandLinks,
        });
        console.log(devicesLinks);
    }
    utils.saveToJsonFile('devicesLinks', devicesLinks);
    return devicesLinks;
}
function getDevicesLinksFromCategoryPage(categoryPageUrl) {
    return getContent(categoryPageUrl)
        .then(body => {
            const $ = cheerio.load(body);
            const devices = $('.makers').find('ul').children();
            const links = [];
            devices.each(function (index, element) {
                const $a = $(element).find('a');
                const name = $a.find('strong span').text();
                const link = baseUrl + '/' + $a.attr('href');
                links.push(link);
            });
            return links;
        });
}