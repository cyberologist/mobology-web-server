const fs = require('fs')
const utils = require('../utils');
const Mobile = require('../../models/mobile');
const Brand = require('../../models/brand');
var valuesGetter = require('sequelize-values')();

const generateDevices = async (array) => {


    const localHits = 0;
    const rates = 0;
    const totalRates = 0;
    const rating = 2.5;
    const localFans = 0;

    array.forEach(mobile => {
        const mobileSpecsFormatted = {};
        for (key in mobile.specs) {
            for (innerKey in mobile.specs[key]) {
                mobileSpecsFormatted[innerKey] = mobile.specs[key][innerKey];
            }
        }

        Mobile.create({
            model: mobile.model,
            brand: mobile.brand,
            localHits: localHits,
            globalHits: (mobile.globalHits) ? (mobile.globalHits.replace(/,/g, '')) : 0,
            hits: Number(localHits) + Number(mobile.globalHits.replace(/,/g, '')),
            rates: rates,
            totalRates: totalRates,
            rating: rating,
            localFans: localFans,
            globalFans: Number(mobile.globalFans.replace(/,/g, '')),
            fans: Number(mobile.globalFans.replace(/,/g, '')) + Number(localFans),
            smallImageUrl: mobile.smallImageUrl,
            reviewLink: mobile.reviewLink,
            images: JSON.stringify(mobile.images),
            specs: JSON.stringify(mobileSpecsFormatted),
        })
    })
    console.log('refreshed deivces    : ', array.length);
}
const generateBrands = async (array) => {
    let brands = [];
    array.forEach(mobile => {
        brands.push(mobile.brand);
    });
    brands = brands.filter((v, i, a) => a.indexOf(v) === i);
    brands.forEach(b => {
        Brand.create({
            name: b,
            logo: `mobology.herokuapp.com/images/logos/${b}.png`
        });
    })
    console.log('refreshed brands   : ', brands.length);

}
const generateData = async () => {

    const data = await utils.getDataFromFile('../all_devices.json');
    const array = JSON.parse(data).filter(d => nullFilter(d) ? true : false);

    await generateBrands(array);
    await generateDevices(array);

    console.log('done.');

}
const nullFilter = (mobile) => {
    // console.log(mobile.reviewLink);
    // return ((typeof mobile.reviewLink !== 'undefined'));
    return (mobile.model &&
        mobile.brand &&
        mobile.globalHits &&
        mobile.globalFans &&
        mobile.smallImageUrl &&
        mobile.reviewLink &&
        mobile.images &&
        mobile.specs
    );
}
// generateData();
module.exports = generateData; 