const csv = require('csv-parser')
const fs = require('fs')

const Mobile = require('../models/mobile');

const results = [];
fs.createReadStream('phone_dataset.csv')
    .pipe(csv())
    .on('data', (data) => results.push(data))
    .on('end', () => {
        const filtered = results.filter(device => {
            for (var key in device) {
                if (!device[key] || device[key] == null || device[key] == '-' || device[key] == '') {
                    return false;
                }
            }
            return true;
        })

        console.log('before : ', results.length, ', after : ', filtered.length);

        const views = Math.floor(Math.random() * Math.floor(101));
        const rates = 0;
        const totalRates = 0;
        const rating = 2.5;
        filtered.forEach(mobile => {
            // console.log(mobile[0]);
            Mobile.create({
                brand: mobile['brand'] == null ? mobile['0'] : mobile['brand'],
                model: mobile['model'] == null ? mobile['1'] : mobile['model'],
                views: views,
                rates: rates,
                totalRates: totalRates,
                rating: rating,
                networkTechnology: mobile['network_technology'] == null ? mobile['2'] : mobile['network_technology'],
                secondGBands: mobile['2G_bands'] == null ? mobile['3'] : mobile['2G_bands'],
                thirdGBands: mobile['3G_bands'] == null ? mobile['4'] : mobile['3G_bands'],
                fourGBands: mobile['4G_bands'] == null ? mobile['5'] : mobile['4G_bands'],
                networkSpeed: mobile['network_speed'] == null ? mobile['6'] : mobile['network_speed'],
                gprs: mobile['GPRS'] == null ? mobile['7'] : mobile['GPRS'],
                edge: mobile['EDGE'] == null ? mobile['8'] : mobile['EDGE'],
                announced: mobile['announced'] == null ? mobile['9'] : mobile['announced'],
                status: mobile['status'] == null ? mobile['10'] : mobile['status'],
                dimentions: mobile['dimentions'] == null ? mobile['11'] : mobile['dimentions'],
                weight: mobile['weight_g'] == null ? mobile['12'] : mobile['weight_g'],
                //wight_oz
                sim: mobile['SIM'] == null ? mobile['14'] : mobile['SIM'],
                displayType: mobile['display_type'] == null ? mobile['15'] : mobile['display_type'],
                displayResolution: mobile['display_resolution'] == null ? mobile['16'] : mobile['display_resolution'],
                displaySize: mobile['display_size'] == null ? mobile['17'] : mobile['display_size'],
                operatingSystem: mobile['OS'] == null ? mobile['18'] : mobile['OS'],
                cpu: mobile['CPU'] == null ? mobile['19'] : mobile['CPU'],
                chipset: mobile['Chipset'] == null ? mobile['20'] : mobile['Chipset'],
                gpu: mobile['GPU'] == null ? mobile['21'] : mobile['GPU'],
                memoryCard: mobile['memory_card'] == null ? mobile['22'] : mobile['memory_card'],
                internalMemory: mobile['internal_memory'] == null ? mobile['23'] : mobile['internal_memory'],
                ram: mobile['RAM'] == null ? mobile['24'] : mobile['RAM'],
                primaryCamera: mobile['primary_camera'] == null ? mobile['25'] : mobile['primary_camera'],
                secondaryCamera: mobile['secondary_camera'] == null ? mobile['26'] : mobile['secondary_camera'],
                loudSpeaker: mobile['loud_speaker'] == null ? mobile['27'] : mobile['loud_speaker'],
                audioJack: mobile['audio_jack'] == null ? mobile['28'] : mobile['audio_jack'],
                waln: mobile['WLAN'] == null ? mobile['29'] : mobile['WLAN'],
                bluetooth: mobile['bluetooth'] == null ? mobile['30'] : mobile['bluetooth'],
                gps: mobile['GPS'] == null ? mobile['31'] : mobile['GPS'],
                nfc: mobile['NFC'] == null ? mobile['32'] : mobile['NFS'],
                radio: mobile['radio'] == null ? mobile['33'] : mobile['radio'],
                usp: mobile['USB'] == null ? mobile['34'] : mobile['USP'],
                sensors: mobile['sensors'] == null ? mobile['35'] : mobile['sensors'],
                battery: mobile['battery'] == null ? mobile['36'] : mobile['battery'],
                colors: mobile['colors'] == null ? mobile['37'] : mobile['colors'],
                approxPriceEUR: mobile['approx_price_EUR'] == null ? mobile['38'] : mobile['approx_price_EUR'],
                imgUrl: mobile['img_url'] == null ? mobile['39'] : mobile['img_url']
            })
        })


        // fs.writeFile('json_phones_dataset.json', JSON.stringify(filtered), err => {
        //     if (err) {
        //         console.log(err);
        //     }
        // });
    });

