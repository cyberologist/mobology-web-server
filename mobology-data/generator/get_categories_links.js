const utils = require('./utils');
const baseUrl = 'https://www.gsmarena.com/';
const cheerio = require('cheerio');
// @return [{brand : 'brand' , links : ['ww..]}]
module.exports.getCategoriesLinks = async () => {

    // console.log('extracting brands pages links...');

    const data = await getHomeCategoryLinks();

    const fullCategoriesLinks = [];

    for (let i = 0; i < data.length; i++) {
        const firstLink = (data[i]['link']);
        const brand = (data[i]['brand']);
        let allLinks;

        console.log('    - ' + (i + 1) + '/' + data.length + ' extracting ' + brand + ' pagesLinks..');
        try {
            allLinks = await getCategoryLinks(firstLink, brand);
        } catch (err) {
            if (err) {
                console.log(err);
                i--;
                continue;
            }
        }
        console.log('      done');
        const linksToAdd = { brand: data[i]['brand'], links: allLinks };
        fullCategoriesLinks.push(linksToAdd);
    }

    console.log('done');

    return fullCategoriesLinks;
}

// @return [{ brand : 'brand' , link : 'www...'}]
function getHomeCategoryLinks() {

    console.log('extracting brands firstPages link..');

    return utils.getContent(baseUrl)
        .then(body => {
            const links = [];
            const $ = cheerio.load(body);
            const categoriesList = $('.brandmenu-v2.light.l-box.clearfix').find('ul').children();
            categoriesList.each(function (index, element) {
                links.push({
                    brand: $(element).text(),
                    link: baseUrl + '/' + $(element).find('a').attr('href')
                })
            });

            console.log('done');

            return links;
        })
}
// @return ['www...' , 'www...' ]
function getCategoryLinks(categoryFirstPage, brand) {


    return getContent(categoryFirstPage)
        .then(body => {
            const allLinks = [categoryFirstPage];
            const $ = cheerio.load(body);
            const navPages = $('.nav-pages').find('a');
            navPages.each(function (index, element) {
                const $a = $(element);
                allLinks.push(baseUrl + '/' + $a.attr('href'));
            })

            return allLinks;
        })
}
