module.exports = function getLatestDevices(url) {
    return getContent(url).then(
        body => {
            const latest = [];
            const inStoreNow = [];
            const result = [latest, inStoreNow];

            const $ = cheerio.load(body);
            // const latest = $('.module module-phones module-instores');

            const categories = $('.module-phones-link');
            categories.each(function (i, element) {
                const $element = $(element);
                const link = $element.attr('href');
                const img = $element.find('a img').attr('src');
                ((i < 6) ? latest : inStoreNow).push({
                    name: $element.text(),
                    link: link,
                    imgUrl: img
                });
            })
            return result;
        }
    )
}
