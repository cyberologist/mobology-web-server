// const axios = require('axios');
const DevicesLinks = require('./get_devices_links');
const Device = require('./get_device');
const utils = require('./utils');
const fs = require('fs');


async function scrapGSM() {

    const devices = [];

    console.log('started : ' + utils.getCurrentTime());


    const linksfile = await getDataFromFile('devicesLinks.json', 'utf8');
    const categories = JSON.parse(linksfile);
    //await DevicesLinks.getDevicesLinks();

    for (let i = 0; i < categories.length; i++) {
        const brand = categories[i].brand;
        const brandLinks = categories[i].brandLinks;

        console.log('extracting ' + (' - ' + i + '/' + categories.length) + ' - : ' + brand + ' devices details');
        for (let j = 0; j < brandLinks.length; j++) {

            const link = brandLinks[j];

            console.log(' - ' + j + '/' + brandLinks.length + ' link : ' + link);
            let device;

            try {
                device = await Device.getDevice(link, brand);
            } catch (err) {
                if (err) {
                    console.log(err);
                    j--;
                    await sleep(2000);
                    continue;
                }
            }

            devices.push(device);
            // await appendToJsonFile('all_devices.json', device);
            await appendToFile('all_devices.text', device);

        }
    }
    console.log('finished : ' + utils.getCurrentTime());
    utils.saveToJsonFile('full_devices', devices);
    return devices;
}
scrapGSM().then((d => console.log(d)));


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}